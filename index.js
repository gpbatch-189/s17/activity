/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function details(){
		let fullName = prompt('Enter your full name')
		let age = prompt('Enter your age')
		let location = prompt('Enter your location')
		console.log('Hello, ' + fullName)
		console.log('You are ' + age + ' years old.')
		console.log('You live in ' + location + ' New York City')
	}

	details()



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function top5FaveMusicArtist(){
		console.log('1. Parokya ni Edgar')
		console.log('2. Eraserheads')
		console.log('3. Coldplay')
		console.log('4. Beatles')
		console.log('5. Queen')
	}

	top5FaveMusicArtist()

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	function top5FaveMovies(){
		console.log('1. Frozen ' )
		console.log('Rotten Tomatoes Rating: 90% ')
		console.log('2. Beauty and the Beast')
		console.log('Rotten Tomatoes Rating: 94% ')
		console.log('3. Finding Nemo')
		console.log('Rotten Tomatoes Rating: 99% ')
		console.log('4. Tangled')
		console.log('Rotten Tomatoes Rating: 89%')
		console.log('5. Shrek')
		console.log('Rotten Tomatoes Rating: 88%')
	}

	top5FaveMovies()

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");
	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


